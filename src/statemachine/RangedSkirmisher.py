import StateMachine
import Physics
import Actions


ATTACK=1
WAIT=2
CHASE=3
BACK = 4
FOLLOW = 5
ALIGN = 6
class RangedSkirmisher(StateMachine.StateMachine):
    def __init__(self):
        self.states = [ATTACK,CHASE,WAIT,BACK,FOLLOW,ALIGN]
        self.start = WAIT
        self.tFunction = transition
        self.aFunction = action
        StateMachine.StateMachine.__init__(self,self.start,self.states,self.tFunction,self.aFunction)
        
def action(current,actor,area,time,delay):
    player = area.getPlayer()
    if current == WAIT:
        pass
    elif current == CHASE:
        #when self.path is set to a path, the character will try to follow it.
        actor.path = area.getPath(actor.clone(),actor.rect.midbottom,player.rect.midbottom,7)
    elif current == ALIGN:
        actor.path = []
        xdiff = player.rect.topleft[0] - actor.rect.topleft[0]
        ydiff = player.rect.topleft[1] - actor.rect.topleft[1]
        direction = [0,0]
        if abs(xdiff) < abs(ydiff):
            if xdiff < 0:
                direction = [-1,0]
                
            else:
                direction = [1,0]
                
        else:
            if ydiff < 0:
                direction = [0,1]
            else:
                direction = [0,-1]
        
        
        Actions.move(actor,area,direction)
        difference = Physics.vectorSub(player.rect.center,actor.rect.center)
        if difference[0] == 0 and difference[1] == 0:
            difference[0] = difference[0] +  1
        if abs(difference[0]) > abs(difference[1]):
            if difference[0] < 0:
                difference = [-1,0]
            else:
                difference = [1,0]
        else:
            if difference[1] < 0:
                difference = [0,-1]
            else:
                difference = [0,1]
        actor.changeDirection(difference)
    elif current == ATTACK:
        
        actor.mainAction(area)
    elif current == BACK:
        #calculate which direction to back up
        difference = Physics.vectorSub(player.rect.center,actor.rect.center)
        difference = Physics.vectorByScalar(difference,-1)
        difference = Physics.normalizeVector(difference)
        if difference[0] == 0 and difference[1] == 0:
            difference[0] = difference[0] +  1
        
        Actions.move(actor,area,difference)
def transition(current,actor,area,time,data):
    
    player = area.getPlayer()
    pTile = area.getTileCoords(player.rect.center)
    eTile = area.getTileCoords(actor.rect.center)
    distanceVector = Physics.vectorSub(eTile,pTile)
    distance = (distanceVector[0]**2 + distanceVector[1]**2)**0.5
    if current == WAIT:
        if distance < 7:
            return CHASE
        else:
            return WAIT
    elif current == CHASE:
        if distance < 1:
            return BACK
	elif distance < 3:
	     return ALIGN
        elif distance > 7:
            return WAIT
        else:
            data['delay'] = time+500
            return FOLLOW
    elif current == FOLLOW:
        if data['delay'] < time:
            return CHASE
        else:
            return FOLLOW
    elif current == ALIGN:
        if player.rect.topleft[0] - actor.rect.topleft[0] <= 5 or player.rect.topleft[1] - actor.rect.topleft[1] <= 5:
            return ATTACK
        elif distance >= 4:
            return CHASE
        elif distance < 1: 
            return BACK
        else:
            return ALIGN
    elif current == ATTACK:
        if distance > 2:
            return CHASE
        elif distance >7:
            return WAIT
        elif distance < 1:
            return BACK
        elif player.rect.topleft[0] - actor.rect.topleft[0] <= 5 or player.rect.topleft[1] - actor.rect.topleft[1] <= 5:
            return ATTACK
        else:
            return CHASE
    elif current == BACK:
        if distance >= 2:
            return CHASE
        elif distance  > 7:
            return WAIT
        else:
            return BACK

