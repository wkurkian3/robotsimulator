class Statistics:
    # Item Related Statistics
    numMeleeItems = 0
    numRangedItems = 0
    numMagicItems = 0
    numOtherItems = 0
    totalItems = 0
    meleeUses = 0
    rangedUses = 0
    magicUses = 0
    otherUses = 0
    totalUses = 0

    # Completionist related statistics
    totalEnemies = 0
    enemiesKilled = 0
    
    # Speed Runner related statistics
    startToGoalDist = 0.0
    timeToComplete = 0.0
    
    # Skill related statistics
    damageTaken = 0
    
    def __init__(self,statistics = None):
        pass
    def displayStats(self):
        print 'Melee Items: ' + str(self.numMeleeItems)
        print 'Ranged Items: ' + str(self.numRangedItems)
        print 'Magic Items: ' + str(self.numMagicItems)
        print 'Other Items: ' + str(self.numOtherItems)
        print 'Total Itemms: ' + str(self.totalItems)
        print 'Melee Uses: ' + str(self.meleeUses)
        print 'Ranged Uses: ' + str(self.rangedUses)
        print 'Magic Uses: ' + str(self.magicUses)
        print 'Other Uses: ' + str(self.otherUses)
        print 'Total Uses: ' + str(self.totalUses)
        print 'Total Enemies: ' + str(self.totalEnemies)
        print 'Enemies Killed: ' + str(self.enemiesKilled)
        print 'Start to Goal Distance: ' + str(self.startToGoalDist)
        print 'Completion Time: ' + str(self.timeToComplete)
        print 'Damage Taken: ' + str(self.damageTaken)
