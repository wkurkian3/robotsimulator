
defaultTileProps = {"height" : [0], "type" : ["floor"], "walkbox" : ["equal"]}
defaultSpriteProps = {"height" : [1.5],"stepup" : [.5], "walkbox" : ["7,26,8,4"]}
defaultAnimationProps = {"sequence" : [0,0,0,0]}
defaultUIProps = {}

TilePropertiesCache = {}
SpritePropertiesCache = None
AnimationPropertiesCache = {}
UIPropertiesCache = {}

def getTileProperties(tilesetname):
    global TilePropertiesCache
    if not tilesetname in TilePropertiesCache.keys():
        props = Properties.getByFile("images/" +tilesetname + ".txt", defaultTileProps)
        TilePropertiesCache[tilesetname] = props
    else:
        props = TilePropertiesCache[tilesetname]
    return props
def getAnimationProperties(animationname):
    global AnimationPropertiesCache
    if not animationname in AnimationPropertiesCache.keys():
        props = Properties.getByFile("images/" +animationname + ".txt", defaultAnimationProps)
        AnimationPropertiesCache[animationname] = props
    else:
        props = AnimationPropertiesCache[animationname]
    return props
def getUIProperties(uiName):
    global UIPropertiesCache
    if not uiName in UIPropertiesCache.keys():
        props = Properties.getByFile("ui/" +uiName + ".txt", defaultUIProps)
        UIPropertiesCache[uiName] = props
    else:
        props = UIPropertiesCache[uiName]
    return props
def getCharacterProperties(spritename):
    global SpritePropertiesCache
    if (SpritePropertiesCache == None):
        props = Properties.getByFile("images/sprites.txt", defaultSpriteProps)
        SpritePropertiesCache = props
    return SpritePropertiesCache.getBranch(spritename)
TILE = getTileProperties
SPRITE= getCharacterProperties
ANIMATION = getAnimationProperties
UI = getUIProperties
def makeProperties(name,kind):
    return kind(name)
class Properties:
    def __init__(self, props):
        self.props = props
    @classmethod
    def getByFile(cls,filename, default = {}):
        f = open(filename)
        line = f.readline()
        lastindex = ""
        props = {}
        while line != "":
            
            if line[0] == ":":
                lastindex = line[1:].strip()
                props[lastindex] = {}
            else:
                sep = line.split("=")
            
                if (len(sep) == 2):
                    if (not sep[0].strip() in props[lastindex].keys()):
                        props[lastindex][sep[0].strip()] = [sep[1].strip()]
                    else:
                        props[lastindex][sep[0].strip()].append(sep[1].strip())
            line = f.readline()
        for i in default.keys():
            for o in props.keys():
                if not i in props[o]:
                    props[o][i] = default[i]
        self = cls(props)
        return self
    @classmethod
    def getDefaultProperties(cls,props):
        self = cls(props)
        return self
    
    def getBranch(self,index):
        return Properties(self.props[index])
    def getStepup(self):
        return float(self.props["stepup"][0])
    def getHeight(self):
        
        return float(self.props["height"][0])
    def getWalkbox(self,image):
        walk = self.props["walkbox"][0].split(",")
        if (walk[0] == "equal"):
            size = image.get_size()
            return [0,0,size[0],size[1]]
        walk = [int(i) for i in walk]
        return walk
    def getArray(self,name):
        sequ = self.props[name][0].split(",")
        sequ = [int(i) for i in sequ]
        return sequ
    def getType(self):
        type = self.props["type"]
        return type
