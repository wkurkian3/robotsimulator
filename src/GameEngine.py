import pygame
import Area
import Sprites
import Character
import Display
import Input
import ImageProc
import Actions
import threading
import UI
import Magic
import argparse
import sys
from MouseAction import *
from Map import *
from generation.WorldGeneration import *
import random
import item.Item as ItemFactory

class GameEngine:
    PLAYING=0
    START=1
    MAP=2
    ARROW_MOVEMENT = False
    def __init__(self):
        
        Display.init([800,600])
        
        self.mouseState = MOUSE_MOVE
        self.mouseFunction = None
        robot = ImageProc.getSprite("robot",[1,1])
        inputMethod = []
        if self.ARROW_MOVEMENT:
            inputMethod = [[pygame.K_UP,[Actions.move,[[0,-1]]]],
                [pygame.K_LEFT,[Actions.move,[[-1,0]]]],
                [pygame.K_DOWN,[Actions.move,[[0,1]]]],
                [pygame.K_RIGHT,[Actions.move,[[1,0]]]],
                 [pygame.K_z,[Actions.useItem,[]]],
                [pygame.K_a,[Actions.pickupItem,[]]]]
        
        self.player = Sprites.Person(robot["images"],Character.Character(),robot["properties"],None, Sprites.navigationFunction)

        samplePicked = False
        self.area = Area.Area(Area.buildMapFile("SRRTest"))
        for y in range(1,len(self.area.iTiles)-1):
            for x in range(1,len(self.area.iTiles[y])-1):
                if (self.area.iTiles[y][x] == None):
                    if random.random() <= .1 and not samplePicked:
                        samplePicked = True
                        sample = ItemFactory.buildStatUp(0,1)
                        drop = Sprites.Item(sample.getIcon(),sample)
                        tilePosition = self.area.getTilePosition([x,y],[0,0],16)
                        drop.rect.topleft = tilePosition
                        self.area.setItems(self.area.items + [drop])
                        

        #generator = WorldGenerator()
        #self.area = generator.generateVillage(generator.generateEncoding())
        self.area.setActors([self.player])
        Display.setArea(self.area)
        
        self.areas = Map([self.area],[250,250])
        
        self.target = self.player
        
    def updateMouseState(self,mouseAction):
        self.mouseState = mouseAction
    def mouseMoveSelect(self,player):
        self.target.ai = True
        self.target =player
        player.ai =False
        Display.setPlayer(self.target)
    def mouseClicked(self):
        Display.setMouse(list(self.mouse))
        personSelected = False
        for i in self.area.actors:
            if i.rect.collidepoint(self.mouse[1]):
                personSelected = True
                if self.mouseState == MOUSE_SELECT or self.mouseState == MOUSE_CAST:
                    if not self.mouseFunction == None:
                        self.mouseFunction(i)
    def checkMouseState(self):
        selected = False
        for i in self.area.actors:
            if i.rect.collidepoint(self.mouse[1]):
                self.mouseState = MOUSE_SELECT
                self.mouseFunction = self.mouseMoveSelect
                selected = True
        if not selected:
            self.mouseState = MOUSE_MOVE
            self.mouseFunction = None
    
    
    def startScreen(self):
        Input.checkInput()
        self.keys = Input.getKeys()
        
        self.mouse = Input.getMouseState(True)
        
        nextMap = None
        if self.keys[pygame.K_RETURN]:
            self.__init__()
            self.currentState = self.PLAYING
            
        Display.showStart()
        
        #Display.wait(20)
        return Input.quitPressed()
    def loop(self):
        Input.checkInput()
        self.keys = Input.getKeys()
        
        if not self.ARROW_MOVEMENT:
            self.mouse = Input.getMouseState(True)
            self.mouse = Display.transformMouse(self.mouse)
        else:
            self.mouse = [False,[0,0]]
        self.checkMouseState()
        Display.setMouse(self.mouse)
        Display.updateOffset(self.keys,self.ARROW_MOVEMENT)
        self.moveOkay=True
        if self.mouse[0]:
            self.mouseClicked()
        for actor in self.area.actors:
            actor.decide(self.area,pygame.time.get_ticks(), self.keys,list(self.mouse))
            
        nextMap = None
        if self.keys[pygame.K_SPACE] and self.area.canTransfer():
            pass
        
        Display.redraw()
        

        #Display.wait(20)
        return Input.quitPressed()
            
    def stop(self):
        #self.map.stop()
        Display.stop()
        

    def run(self):
        done =False
        self.currentState = self.PLAYING
        while(done == False):
            if self.currentState == self.PLAYING:
                cont = self.loop()
            else:
                cont = self.startScreen()
            done= cont

        self.stop()
    
def run():
    game = GameEngine()
    game.run()
if __name__ == "__main__":
    if "profile" in sys.argv:
        import hotshot
        import hotshot.stats
        import tempfile
        import os
 
        profile_data_fname = tempfile.mktemp("prf")
        try:
            prof = hotshot.Profile(profile_data_fname)
            prof.run('run()')
            
            del prof
            
            s = hotshot.stats.load(profile_data_fname)
            s.strip_dirs()
            print "cumulative\n\n"
            s.sort_stats('cumulative').print_stats()
            print "By time.\n\n"
            s.sort_stats('time').print_stats()
            del s
        finally:
            # clean up the temporary file name.
            try:
                os.remove(profile_data_fname)
            except:
                # may have trouble deleting ;)
                pass
    else:
        
        run()
