from Statistics import Statistics
import PlayerModel

instance = None

def init():
    global instance
    instance = Statistics()

def addLevelStats(lvlStats):
    instance.numMeleeItems += lvlStats.numMeleeItems
    instance.numRangedItems += lvlStats.numRangedItems
    instance.numMagicItems += lvlStats.numMagicItems
    instance.numOtherItems += lvlStats.numOtherItems
    instance.totalItems += lvlStats.totalItems

    instance.meleeUses += lvlStats.meleeUses
    instance.rangedUses += lvlStats.rangedUses
    instance.magicUses += lvlStats.magicUses
    instance.otherUses += lvlStats.otherUses
    instance.totalUses += lvlStats.totalUses

    instance.totalEnemies += lvlStats.totalEnemies
    instance.enemiesKilled += lvlStats.enemiesKilled

    instance.startToGoalDist += lvlStats.startToGoalDist
    instance.timeToComplete += lvlStats.timeToComplete

    instance.damageTaken += lvlStats.damageTaken

def display():
    global instance
    instance.displayStats()

def generateAccumPlayer():
    pm = PlayerModel.PlayerModel(instance)
    return pm

def adaptModelOverall(playerM):
    accumPM = generateAccumPlayer()
    accumPM.meleePref = (accumPM.meleePref + 2 * playerM.meleePref) / 3.0
    accumPM.rangedPref = (accumPM.rangedPref + 2 * playerM.rangedPref) / 3.0
    accumPM.magicPref = (accumPM.magicPref + 2 * playerM.magicPref) / 3.0
    accumPM.otherPref = (accumPM.otherPref + 2 * playerM.otherPref) / 3.0

    accumPM.skill = (accumPM.skill + 2 * playerM.skill) / 3.0
    accumPM.completionist = (accumPM.completionist + 2 * playerM.completionist) / 3.0
    accumPM.speedrunner = (accumPM.speedrunner + 2 * playerM.speedrunner) / 3.0
    return accumPM
