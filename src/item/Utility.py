import Item
import ImageProc
import Physics
import Display
import pygame
import pygame.transform

class GrapplingHook(Item.Item):
    attack = 0
    defense = 0
    speed = 0
    images = []
    frame = 0
    FORWARD=0
    RETRACT=1
    PULL=2
    def __init__(self,speed):
        self.type = Item.Item.OTHER
        self.time = 20-min(speed,15)
        self.currentState = self.FORWARD
        self.speed = speed

        tempImages = ImageProc.getAnimation('chain')
        tempImages2 = ImageProc.getAnimation('hook')
        self.size = [tempImages['size'],tempImages2['size']]

        self.images = tempImages['images'] + tempImages2['images']
        
        self.props = [tempImages['props'],tempImages2['props']]
        self.hit = []
        self.travel = 5
        self.count = 0
        self.pos = None
        self.hitbox = None
    def getIcon(self,name =None):
        if name == None:
            name = 'hookicon'
        return ImageProc.getIcon(name)
    def doEffect(self,source,area):
        self.count += 1
        if self.pos == None:
            
            ind =source.currentImageIndex
            center = [int(self.size[0][0]*0.3),int(self.size[0][1]*0.4)]
            hitcenter = [int(self.size[0][0]*0.6),int(self.size[0][1]*0.3)]
            
            if ind == 0:
                self.rotation = 90
                self.direction = [0,-1]
                hitcenter = [int(self.size[0][0]*0.39),int(self.size[0][1]*1.5)]
            
            elif ind == 1:
                self.rotation = 0
                self.direction = [1,0]
                hitcenter = [int(self.size[0][0]*0.39),int(self.size[0][1]*0.5)]
            elif ind == 2:
                self.rotation = -90
                self.direction = [0,1]
                hitcenter = [int(self.size[0][0]*0.23),int(self.size[0][1]*1.25)]
            
            elif ind == 3:
                self.rotation = -180
                self.direction = [-1,0]
                hitcenter = [int(self.size[0][0]*0.53),int(self.size[0][1]*0.7)]
            
            self.pos = source.rect.center
            self.hitbox = source.rect.midbottom
            self.pos = Physics.vectorSub(self.pos,center)
            self.startpos = self.pos
            self.hitbox = Physics.vectorSub(self.hitbox,hitcenter)
        circleBox = None
        if self.currentState == self.FORWARD:
        
            hitbox = self.props[1].getArray('hitbox')
            
            if self.rotation == 0: 
                circleBox = pygame.Rect(hitbox[0],hitbox[1],hitbox[2],hitbox[3])
            elif self.rotation == -180:
                circleBox = pygame.Rect(self.size[1][0]-hitbox[0],self.size[1][1]-hitbox[1],hitbox[2],hitbox[3])
            elif self.rotation == -90: 
                circleBox = pygame.Rect(hitbox[1],hitbox[0],hitbox[3],hitbox[2])
            elif self.rotation == 90:
                circleBox = pygame.Rect(self.size[1][1]-hitbox[1],self.size[1][0]-hitbox[0],hitbox[3],hitbox[2])
                
            circleBox.topleft = Physics.vectorAdd(circleBox.topleft,self.hitbox)
            
            actors = area.actors
            boxes = [a.rect for a in actors]
            
            indices = circleBox.collidelistall(boxes)
            self.damaged = []
            if len(indices) > 0:
                  for i in indices:
                        actor = actors[i]
                        if not actor is source and not actor in self.hit:
                              self.damaged = self.damaged + [actor]
            
            for actor in self.damaged:
                  if not actor in self.hit:
                        self.hit = self.hit + [actor]
    
        images = []
        positions = []
        distance = Physics.vectorSub(self.pos,self.startpos)
        imageBox = self.props[0].getArray('imagebox')
        slope = [0,0]
        sign = lambda x: (1, -1)[x<0]
        links = 0
        if abs(distance[0]) > abs(distance[1]):
            slope = [sign(distance[0])*imageBox[2],0]
            links = abs(int(distance[0]/slope[0]))
        elif abs(distance[1]) > abs(distance[0]):
            slope = [0,sign(distance[1])*imageBox[2]]
            links = abs(int(distance[1]/slope[1]))
        
        chain = pygame.transform.rotate(self.images[0],self.rotation)
        hook = pygame.transform.rotate(self.images[1],self.rotation)
        length = max(abs(slope[0]),imageBox[3])* links
        height = max(abs(slope[1]),imageBox[2])*links
        
        for i in range(links):
            newpos = Physics.vectorAdd(self.startpos,Physics.vectorByScalar(slope,i))
            newimage = chain.copy()
            positions.append(newpos)
            images.append(newimage)
        
        images.append(hook)
        positions.append(self.pos)
        #if not circleBox == None:
        #    collide = pygame.Surface((circleBox.width,circleBox.height))
        #    collide.fill([255,255,255])
        #    images.append(collide)
       #     positions.append(circleBox.topleft)
        if self.currentState == self.FORWARD:
            distance = self.travel
            offset = Physics.vectorByScalar(self.direction,distance)
            self.pos = Physics.vectorAdd(self.pos,offset)  
            self.hitbox = Physics.vectorAdd(self.hitbox,offset)
            if len(self.hit) > 0 or links > 20:
                self.currentState = self.RETRACT
                if len(self.hit) > 0:
                    self.target = self.hit[0]
                else:
                    self.target = None
                self.hit = []
                return [True,positions,images,self.time]  
            
            elif not area.checkWall(circleBox):
                self.currentState = self.PULL
                return [True,positions,images,self.time]
            
            
            return [True,positions,images,self.time]
        elif self.currentState == self.PULL:
            distance = self.travel
            if not area.canMove(source,self.direction,distance):
                self.stop()
                return [False,positions,images,500]
            else:
                
                offset = Physics.vectorByScalar(self.direction,distance)
                source.rect.topleft = Physics.vectorAdd(source.rect.topleft,offset) 
                self.startpos = Physics.vectorAdd(self.startpos,offset)
                return [True,positions,images,self.time]
        elif self.currentState == self.RETRACT:
            distance = self.travel
            offset = Physics.vectorByScalar(self.direction,distance*-1)
            if not self.target == None:
                self.target.rect.topleft = Physics.vectorAdd(self.target.rect.topleft,offset) 
            self.pos = Physics.vectorAdd(self.pos,offset)  
            if links == 0:
                self.stop()
                return [False,positions,images,500]
            else:
                return [True,positions,images,self.time]
    def stop(self):
        self.frame = 0
        self.hit = []
        self.travel = 5
        self.pos = None
        self.hitbox = None
        self.count = 0
        self.currentState = self.FORWARD

class Shield(Item.Item):
    defense = 0
    speed = 0
    images = []
    def __init__(self,defense):
        self.type = Item.Item.OTHER
        self.defense = defense

    def getIcon(self,name =None):
        if name == None:
            name = 'shield'
        return ImageProc.getIcon(name)
    def doPassiveEffect(self,player):
        player.character.defense += self.defense
    def removePassiveEffect(self,player):
        player.character.defemse -= self.defense
    def doEffect(self,source,area):
        return [False,[0,0],None,1000]
class Beacon(Item.Item):
    def __init__(self):
        pass
    def getIcon(self, name = None):
        if name == None:
            name = "beacon"
        return ImageProc.getIcon(Beacon)
