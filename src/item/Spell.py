import Item
import ImageProc
import Physics
import Display
import pygame

class FrostSpell(Item.Item):
    frame = 0
    attack = 0
    speed = 0
    images = []
    def __init__(self,attack,speed,animation):
        self.type = Item.Item.MAGIC
        self.attack = attack
        self.speed = speed
        self.time = 70
        
        self.frame = 7

        tempImages = ImageProc.getAnimation(animation)
        
        self.size = tempImages['size']

        self.images = tempImages['images']
        
        self.props = tempImages['props']
        self.contact = False
        self.hit = []
        self.travel = 20
        self.count = 0
        self.pos = None
    def getIcon(self,name =None):
        if name == None:
            name = 'frosticon'
        return ImageProc.getIcon(name)
    def doEffect(self,source,area):
        
        if self.pos == None:
            self.pos = source.getHandPosition()
            center = [int(self.size[0]*0.5),int(self.size[1]*0.5)]
            self.pos = Physics.vectorSub(self.pos,center)
            ind =source.currentImageIndex
            if ind == 0:
                self.direction = [0,-1]
            elif ind == 1:
                self.direction = [1,0]
            elif ind == 2:
                self.direction = [0,1]
            elif ind == 3:
                self.direction = [-1,0]
        distance = self.travel
        offset = Physics.vectorByScalar(self.direction,distance)
        self.pos = Physics.vectorAdd(self.pos,offset)  
        

        actors = area.actors
        boxes = [a.rect for a in actors]
        if self.contact:
            circleBox = pygame.Rect(self.pos[0],self.pos[1],self.size[0],self.size[1])
            indices = circleBox.collidelistall(boxes)
            self.damaged = []
            if len(indices) > 0:
                  for i in indices:
                        actor = actors[i]
                        if not actor is source and not actor in self.hit:
                              self.damaged = self.damaged + [actor]
            
            for actor in self.damaged:
                  if not actor in self.hit:
                        dead = actor.damage(self.attack,area)
                        self.hit = self.hit + [actor]
        else:
            circleBox = pygame.Rect(self.pos[0],self.pos[1],self.size[0],self.size[1])
            indices = circleBox.collidelistall(boxes)
            if len(indices) > 0:
                  for i in indices:
                        actor = actors[i]
                        if not actor is source and not actor in self.hit:
                              self.contact = True
                              self.frame = 0

        if self.frame == 0 and self.contact:
            self.travel = 0
            image = self.images[self.frame]
            self.frame = self.frame+1
            return [True,[self.pos],[image],self.time]
        elif self.contact:
            more = True if self.frame < len(self.images) else False
            
            image = None
            if more:
                image = self.images[self.frame]
                self.frame = self.frame+1
            else:
                self.stop()
            return [more,[self.pos],[image],self.time]
        #elif not area.checkWall(circleBox):
         #   
          #  self.stop()
           # return [False,self.pos,None,500]
        else:
            image = self.images[self.frame]
            self.count += 1
            more = True if self.count <= 30 else False
            
            if not more:
                self.stop()
            return [more,[self.pos],[image],self.time]
        
    def stop(self):
        self.frame = 7
        self.hit = []
        self.travel = 20
        self.pos = None
        self.count = 0
        self.contact = False
