import random
from ai.genetic.GeneticAlgorithm import *


def mutateStory(encoding):
    mutation = random.randint(0,2)
    mutated = encoding.clone()
    mutated.addSentence()
    return mutated
def crossoverStory(story1,story2):
    return story1.clone()
def evaluationStory(story):
    points = 0
    sentences = len(story.sentence)
    ratio = sentences/3.0
    if ratio > .75 and ratio < 1.25:
        points += 5
    dead = []
    married = []
    for i in story.sentence:
        if i[1] == "Kill" and i[2] in dead:
            points -= 50
        elif i[1] == "Kill" and not i[2] in dead:
            dead += [i[2]]
        
        if i[1] == "Marry" and i[2] in married:
            points -= 50
        elif i[1] == "Marry" and not i[2] in married:
            married += [i[2]]

        if i[1] == "Marry" and i[2] == i[0]:
            points -= 50
        if i[1] == "Capture" and i[2] == i[0]:
            points -= 50
            

    return points


class Story:
    def __init__(self,sentence,mutate,crossover,evaluate,nouns,verbs):
        self.sentence = sentence
        self.mutate = mutate
        self.crossover = crossover
        self.evaluate = evaluate
        self.nouns = nouns
        self.verbs = verbs
    def generate(self):
        alg = GeneticAlgorithm(self,self.evaluate,self.mutate,self.crossover)
        solution = alg.solve(200)
        solution.display()
    def display(self):
        for i in self.sentence:
            print i
    def addSentence(self):
        nouns = list(self.nouns)
        verbs = list(self.verbs)
        subject = nouns[random.randrange(0,len(nouns))]
        verb = verbs[random.randrange(0,len(verbs))]
        receiver = nouns[random.randrange(0,len(nouns))]
        self.sentence = [[subject,verb,receiver]] + self.sentence
    def clone(self):
        return Story(list(self.sentence),self.mutate,self.crossover,self.evaluate,self.nouns,self.verbs)
    
nouns = ["Hero","Villain", "Princess"]
verbs = ["Kill","Marry","Capture"]

sentence = [[nouns[0],verbs[1],nouns[2]]]

story = Story(sentence,mutateStory,crossoverStory,evaluationStory,nouns,verbs)
story.generate()
