import pygame
import Physics

class Camera(object):
    def __init__(self, camera_func, width, height,winSize):
        self.camera_func = camera_func
        self.state = pygame.Rect(0, 0, width, height)
        self.winSize = winSize

    def apply(self, target):
        return target.rect.move(self.state.topleft)
    def applyCoords(self,coords):
        return Physics.vectorAdd(coords,self.state.topleft) 

    def update(self, target):
        self.state = self.camera_func(self.state, target,self.winSize)
    def reverseCoords(self,coords):
        return Physics.vectorSub(coords,self.state.topleft)
def simple_camera(camera, target_rect,winSize):
    l, t = target_rect
    _, _, w, h = camera
    return pygame.Rect(-l+0.5*winSize[0], -t+0.5*winSize[1], w, h)

def complex_camera(camera, target_rect,winSize):
    l = target_rect[0]
    t = target_rect[1]
    _, _, w, h = camera
    l, t, _, _ = -l+winSize[0]*0.5, -t+winSize[1]*0.5, w, h

    l = min(0, l)                           # stop scrolling at the left edge
    l = max(-(camera.width-winSize[0]), l)   # stop scrolling at the right edge
    t = max(-(camera.height-winSize[1]), t) # stop scrolling at the bottom
    t = min(0, t)                           # stop scrolling at the top
    return pygame.Rect(l, t, w, h)

