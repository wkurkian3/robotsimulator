# We'll probably use a generalized PlayerModel rather than subclasses
class PlayerModel:
    #Item Related Player Descriptors
    meleePref = 0.0
    rangedPref = 0.0
    magicPref = 0.0
    otherPref = 0.0

    #Archetype Related Player Descriptors
    skill = 0.0
    completionist = 0.0
    speedrunner = 0.0
    
    def __init__(self,statistics = None):
        #raise NotImplementedError("Subclasses should implement this!")
        if (statistics):
            if not statistics.totalEnemies == 0:
                killRate = statistics.enemiesKilled / statistics.totalEnemies
            else:
                killRate = 0
            #self.meleePref = self.calculateItemPref(statistics.meleeUses, statistics.totalUses, statistics.numMeleeItems)
            #self.rangedPref = self.calculateItemPref(statistics.rangedUses, statistics.totalUses, statistics.numRangedItems)
            #self.magicPref = self.calculateItemPref(statistics.magicUses, statistics.totalUses, statistics.numMagicItems)
            #self.otherPref = self.calculateItemPref(statistics.otherUses, statistics.totalUses, statistics.numOtherItems)
            self.meleePref = self.fuzzItem(statistics.meleeUses, statistics.totalUses, statistics.numMeleeItems, statistics.totalItems)
            self.rangedPref = self.fuzzItem(statistics.rangedUses, statistics.totalUses, statistics.numRangedItems, statistics.totalItems)
            self.magicPref = self.fuzzItem(statistics.magicUses, statistics.totalUses, statistics.numMagicItems, statistics.totalItems)
            self.otherPref = self.fuzzItem(statistics.otherUses, statistics.totalUses, statistics.numOtherItems, statistics.totalItems)

    def getPlayerDescription(self):
        raise NotImplementedError("Subclasses should implement this!")

    def calculateItemPref(self,numUses, totUses, totItems):
        useRate = 0.0
        if not totUses == 0:
            useRate = numUses / totUses
        
        usePossible = 0.0
        # If there is a non-zero number of melee items, we can have some confidence in result
        if (totItems > 0):
            usePossible = 1.0
        
        itemPref = min(usePossible, useRate)
        return itemPref

    def fuzzItem(self,numUses, totUses, numOfType, numTotal):
        if not totUses == 0:
            usePercent = float(numUses)/float(totUses)
        else:
            usePercent = 0.0
        if not numTotal == 0:
            itemRate = float(numOfType)/float(numTotal)
        else:
            itemRate = 0.0
        #print itemRate
        # Relational and Rule Sets (Variables)
        # Reliance = {Unreliant, Neutral, Reliant}
        reliance = self.relianceMembership(usePercent)
        #print(reliance)
        # Opportunity = {Low Opportunity, Fair Opportunity, High Opportunity}
        opportun = self.opportunityMembership(itemRate)
        #print(opportun)
        # Strength = {Weak, Average, Strong}
        strength = self.strengthFunction(reliance, opportun)
        #print(strength)
        # Opinion = {Negative, Indifferent, Positive}
        #opinion = self.opinionFunction(reliance)
        opinion = (reliance)
        #print(opinion)

        # Accumulate
        accum = self.accumItemPref(strength, opinion)
        #print("Accumulation:")
        #print(accum)
        itemPref = self.defuzzItemPref(accum)
        return itemPref

    def relianceMembership(self, measure):
        reliance = [0.0, 0.0, 0.0]
        # Unreliant
        if (measure <=0.2):
            reliance[0] = 1.0
        else:
            reliance[0] = max(0.0, 1.0 - (measure - 0.2)/0.2)
        # Neutral
        if (measure <=0.4):
            reliance[1] = max(0.0, (measure - 0.2)/0.2)
        elif (measure >0.4 and measure <0.6):
            reliance[1] = 1.0
        else:
            reliance[1] = max(0.0, 1.0 - (measure - 0.6)/0.2)
        # Reliant
        if (measure <=0.8):
            reliance[2] = max(0.0, (measure - 0.6)/0.2)
        else:
            reliance[2] = 1.0
        return reliance

    def opportunityMembership(self,measure):
        opportunity = [0.0, 0.0, 0.0]
        # Low
        if (measure <=0.2):
            opportunity[0] = 1.0
        else:
            opportunity[0] = max(0.0, 1.0 - (measure - 0.2)/0.2)
        # Fair
        if (measure <=0.4):
            opportunity[1] = max(0.0, (measure - 0.2)/0.2)
        elif (measure >=0.6):
            opportunity[1] = max(0.0, 1.0 - (measure - 0.6)/0.2)
        else:
            opportunity[1] = 1.0
        # High
        if (measure <=0.8):
            opportunity[2] = max(0.0, (measure - 0.6)/0.2)
        else:
            opportunity[2] = 1.0

        return opportunity

    # Not sure if this is more of a Rule or a Membership Function
    def opinionFunction(self,reliance):
        opinion = [0.0, 0.0, 0.0]
        # Negative
        opinion[0] = max(reliance[0],reliance[1])
        # Indifferent
        opinion[1] = reliance[2]
        # Positive
        opinion[2] = max(reliance[3], reliance[4])

        return opinion

    def strengthFunction(self, reliance, opportunity):
        strength = [0.0, 0.0, 0.0]
        # strengthLeft = (a OR b)
        for x in xrange(0, 3):
            strength[x] = max(opportunity[x], reliance[x])
        return strength

    def accumItemPref(self,strength, opinion):
        fuzzyPref = [0.0, 0.0, 0.0, 0.0, 0.0]
        tempArray = [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for x in xrange(0, 3):
            for y in xrange(0, 3):
                tempArray[x][y] = min(strength[x], opinion[y])

        fuzzyPref[0] = (tempArray[1][0]/3.0) + 2 * (tempArray[2][0]/3.0)
        fuzzyPref[1] = 2 * (tempArray[1][0]/5.0) + 2 * (tempArray[0][0]/5.0) + (tempArray[0][1]/5.0)
        fuzzyPref[2] = (tempArray[0][0]/10.0) + (tempArray[0][1]/10.0) + (tempArray[0][2]/10.0) + 3 * (tempArray[1][1]/10.0) + 4 * (tempArray[2][1]/10.0)
        fuzzyPref[3] = (tempArray[0][0]/5.0) + 2 * (tempArray[2][0]/5.0) + 2 * (tempArray[2][1]/5.0)
        fuzzyPref[4] = (tempArray[2][1]/3.0) +  2 * (tempArray[2][2]/3.0)

        normalizer = max(fuzzyPref)
        for x in xrange(0, 5):
            fuzzyPref[x] = fuzzyPref[x] / normalizer
        
        return fuzzyPref

    def defuzzItemPrefOrig(self,fuzzyPref):
        maxMember = max(fuzzyPref)
        numMax = 0
        sumMax = 0
        for x in xrange(0, 5):
            if (fuzzyPref[x] == maxMember):
                numMax += 1
                sumMax += (0.25 * x)
        #print(numMax)
        #print(sumMax)
        if (numMax > 0):
            return (sumMax / numMax)
        else:
            return 0

    def defuzzItemPref(self, fuzzyPref):
        outMem = self.outputMembership(fuzzyPref)
        topValues = [0.0, 0.0, 0.0, 0.0, 0.0]
        for x in xrange(0, 5):
            topValues[x] = fuzzyPref[x] * outMem[x]
        dividend = max(topValues) - min(topValues)
        divisor = max(outMem) - min(topValues)
        if (divisor > 0):
            return dividend/divisor
        else:
            return 0.2714 # Temporary, because I'm not sure how to handle this case. Attempt to make it easy to identify

    # Turns out this isn't as easy to make a graph out of. But it works out more nicely
    def outputMembership(self, fuzzyPref):
        outMem = [0.0, 0.25, 0.5, 0.75, 1.0]

        #outMem[0] = max(0.0, min(1.0, 2.0 - 20.0 * abs(fuzzyPref[0] - 0.05)))
        #outMem[1] = max(0.0, min(1.0, 2.0 - 20.0 * abs(fuzzyPref[0] - 0.275)))
        #outMem[2] = max(0.0, min(1.0, 2.0 - 20.0 * abs(fuzzyPref[0] - 0.50)))
        #outMem[3] = max(0.0, min(1.0, 2.0 - 20.0 * abs(fuzzyPref[0] - 0.725)))
        #outMem[4] = max(0.0, min(1.0, 2.0 - 20.0 * abs(fuzzyPref[0] - 0.95)))

        return outMem

    def display(self):
        print 'Melee Item Preference: ' + str(self.meleePref)
        print 'Ranged Item Preference: ' + str(self.rangedPref)
        print 'Magic Item Preference: ' + str(self.magicPref)
        print 'Other Item Preference: ' + str(self.otherPref)

# Test functions below____________________________________________________________

# This function gives a melee-focused preset output for a PlayerModel for testing purposes.
def testMeleeDescription():
    testPlayer = PlayerModel()
    testPlayer.meleePref = 0.8
    testPlayer.rangedPref = 0.1
    testPlayer.magicPref = 0.0
    testPlayer.otherPref = 0.0
    testPlayer.skill = 0.2
    testPlayer.completionist = 0.0
    testPlayer.speedrunner = 0.5
    return testPlayer

# This function gives a range-focused preset output for a PlayerModel for testing purposes.
def testRangedDescription():
    testPlayer = PlayerModel()
    testPlayer.meleePref = 0.15
    testPlayer.rangedPref = 0.9
    testPlayer.magicPref = 0.0
    testPlayer.otherPref = 0.0
    testPlayer.skill = 0.5
    testPlayer.completionist = 0.1
    testPlayer.speedrunner = 0.0
    return testPlayer

# Player model for first level
def initialPlayerModel():
    player = PlayerModel()
    player.meleePref = 0.0
    player.rangedPref = 0.0
    player.magicPref = 0.0
    player.otherPref = 0.0
    player.skill = 0.0
    player.completionist = 0.0
    player.speedrunner = 0.0
    return player
