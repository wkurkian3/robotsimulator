import Sprites
import string
import ImageProc
import Physics
import Properties
import copy
import Search
import pygame

def detLayer(pos,size,properties):
    typemodifier = 1
    if "floor" in properties.getType():
        typemodifier = 0
    elif "wall" in properties.getType():
        typemodifier = 1
    ret =int((pos[1]) * typemodifier)
    return ret

class AreaLink():
    def __init__(self, transformString):
        xString = transformString[0]
        yString = transformString[1]
        self.x, self.xtrans = self.getStringProps(xString)
        self.y, self.ytrans = self.getStringProps(yString)
        
    def getStringProps(self,xString):
        x = "0"
        xtrans = "add"
        if xString[0] == "-" and xString[1] == ">":
            x = int(xString[2:])
            xtrans = "equal"
        elif xString[0] == "-":
            x = -1*int(xString[1:])
            xtrans = "add"
        elif xString[0] == "+":
            x = int(xString[1:])
            xtrans = "add"
            
        return x,xtrans
    def transform(self, coordIn):
        coord = list(coordIn)
        if self.xtrans == "add":
            coord[0] += self.x
        elif self.xtrans == "equal":
            coord[0] = self.x
        if self.ytrans == "add":
            coord[1] += self.y
        elif self.ytrans == "equal":
            coord[1] = self.y
        #coord[0]+= 1
        #coord[1] += 1
        return coord
    
def representsInt(num):
    try:
        int(num)
        return True;
    except ValueError:
        return False;

mapfiles = "areas/"
doorsyms = ["+","!","@","#","$","%","^","&","*","(",")","_","~","/",">","<"]
def buildMapFile(*args,**kwargs):
    filename =args[0]
    tiledictBase = None
    tiledictNameBase = None
    tiledictMid = None
    tiledictNameMid = None
    if (len(args) == 2):
        tiledict = args[1]
    f = open(mapfiles+filename+".area")
    line = "s"
    lineno = 0
    area = []
    walls = []
    size = 0
    start = (-1,-1)
    symbols = {}
    connections= {}
    mode = "check"
    while (line != ""):
        line = f.readline()
        if line == "":
            break
        
        #print line
        if mode == "check":
            
            if line[0] == "\"":
                sep = line[1:].split("=")
                #connections["connections"].append(sep[0].strip())
                connections[sep[0].strip()].append(sep[1].strip())
            elif line[0] == "o":
                sep = line[1:].split("=")
                coords = (sep[1].strip()).split(",")
                areaLink = AreaLink(coords)
                connections[sep[0].strip()].append(areaLink)
            elif line[0] =="\'":
                sep = line[1:].split("=")
                
                if tiledictBase == None and sep[0].strip() == "tiledictbase":
                    tiledictNameBase = sep[1].strip()
                elif tiledictMid == None and sep[0].strip() == "tiledictmid":
                    tiledictNameMid = sep[1].strip()
                
            elif line[0] == ":":
                if line[1:].strip() == "base":
                    mode = "base"
                    lineno = 0
                elif line[1:].strip() == "mid":
                    mode = "mid"
                    lineno=0
                
        elif mode == "base":
            indices = line.split()
            if line[0] == ":":
                if line[1:].strip() == "base":
                    mode = "base"
                    lineno = 0
                elif line[1:].strip() == "mid":
                    mode = "mid"
                    lineno=0
                else:
                    mode = "check"
                continue
            area.append([])
            for x in range(len(indices)):
                index = indices[x]
                #print indices
                if index[0] in doorsyms:
                    if not index[0] in connections.keys():
                        connections[index[0]] = [[]]
                    connections[index[0]][0].append([lineno,x])
                if tiledictBase == None:
                    tiledictBase = ImageProc.getTileset(tiledictNameBase,dim=[32,32])
                    basesize = tiledictBase["size"]
                
                if representsInt(index):
                    if int(index) == -1:
                        index = '-'
                if len(index) > 1 and index[0] == '-':
                        start = [x*basesize[0],lineno*basesize[1]]
                if representsInt(index):
                    image = ImageProc.getEntry(tiledictBase, abs(int(index)))
                else:
                    image = ImageProc.getEntry(tiledictBase, index)
                #print index
                props = tiledictBase[image]
                pos = [x*basesize[0],lineno*basesize[1]]
                #print pos
                #print (pos)
                layer = detLayer(pos,basesize,props)
                #print ("size",size)
                area[lineno].append(Sprites.Tile(pos,image,props,layer))
            lineno+= 1
        elif mode == "mid":
            indices = line.split()
            if line[0] == ":":
                if line[1:].strip() == "base":
                    mode = "base"
                    lineno = 0
                elif line[1:].strip() == "mid":
                    mode = "mid"
                    lineno=0
                else:
                    mode = "check"
                continue
            walls.append([])
            for x in range(len(indices)):
                index = indices[x]
                
                if tiledictMid == None:
                    tiledictMid = ImageProc.getTileset(tiledictNameMid,dim=[16,16])
                    midsize = tiledictMid["size"]
                
                if representsInt(index):
                    if int(index) == -1:
                        index = '-'
                if representsInt(index):
                    image = ImageProc.getEntry(tiledictMid, abs(int(index)))
                else:
                    image = ImageProc.getEntry(tiledictMid, index)
                props = tiledictMid[image]
                    
                pos = [x*midsize[0],lineno*midsize[1]]
                #print (pos)
                layer = detLayer(pos,midsize,props)
                #print ("size",size)
                if index == '-':
                    walls[lineno].append(None)
                else:
                    walls[lineno].append(Sprites.Tile(pos,image,props,layer))
            lineno += 1
        
        
    f.close()
    size = tiledictBase["size"]
    return [[area,walls],start,[basesize,midsize],connections,tiledictBase,filename]

class Area:
    tileindex = 0
    startindex = 1
    tilesizeindex = 2
    connectionsIndex = 3
    nameIndex = 5
    update = False
    icon = None
    def __init__(self, area=[]):
        self.tiles= area[self.tileindex][0]
        self.iTiles =area[self.tileindex][1]
        self.start= area[self.startindex]
        self.tilesize = area[self.tilesizeindex]
        self.actors =[]
        self.connections = area[self.connectionsIndex]
        self.name = area[self.nameIndex]
        self.items = []
        self.icon = ImageProc.getIcon('place')
    def canTransfer(self):
        player = self.getPlayer()
        corners = [player.rect.topleft,player.rect.topright,player.rect.bottomleft,player.rect.bottomright]
        for corner in corners:
            coords = self.getTileCoords(corner)
            tile = self.tiles[coords[0]][coords[1]]
            coords = self.getTileCoords(corner)
            if coords[0] == -1:
                continue
            if 'escape' in tile.props.getType():
                return True
        return False
    def checkWall(self,rect):
        pos = rect.topleft
        size = rect.size
        corners = [pos,Physics.vectorAdd(pos, [size[0],0]),Physics.vectorAdd(pos, [0,size[1]]),Physics.vectorAdd(pos, size)]
        for corner in corners:
            
            coords = self.getTileCoords(corner)
            if coords[0] == -1:
                return False
            
            tiles = [self.iTiles[coords[0]*2][coords[1]*2],self.iTiles[coords[0]*2][coords[1]*2+1],self.iTiles[coords[0]*2+1][coords[1]*2],self.iTiles[coords[0]*2+1][coords[1]*2+1]]
            for tile in tiles:
                if tile == None:
                    continue
                if ('wall' in tile.props.getType()):
                    
                    contact = tile.props.getWalkbox(tile.image)
                    topleft = tile.rect.topleft
                    contact[0] += topleft[0]
                    contact[1] += topleft[1]
                    if Physics.RectContains(contact, corner):
                        return False;
        
        return True

    def canMove(self,actor, direction,step):
        actor.changeDirection(direction)
        pos = actor.rect.topleft
        walkbox = actor.props.getWalkbox(actor.image)
        newpos = Physics.vectorAdd(pos, Physics.vectorByScalar(direction, step))
        newpos = Physics.vectorAdd(newpos, walkbox[:2])
        size = walkbox[2:]
        corners = [newpos,Physics.vectorAdd(newpos, [size[0],0]),Physics.vectorAdd(newpos, [0,size[1]]),Physics.vectorAdd(newpos, size)]
        corners += [Physics.vectorAdd(newpos, [size[0]*.5,0]),Physics.vectorAdd(newpos, [0,size[1]*.5]),Physics.vectorAdd(newpos, [size[0],size[1]*.5]),Physics.vectorAdd(newpos,[size[0]*.5,size[1]])]
        for corner in corners:
            
            coords = self.getTileCoords(corner)
            if coords[0] == -1:
                return False
            tiles = [self.iTiles[coords[0]*2][coords[1]*2],self.iTiles[coords[0]*2][coords[1]*2+1],self.iTiles[coords[0]*2+1][coords[1]*2],self.iTiles[coords[0]*2+1][coords[1]*2+1]]
            for tile in tiles:
                if tile == None:
                    continue
                if (actor.props.getStepup() < tile.props.getHeight() or tile.props.getHeight() < 0):
                    contact = tile.props.getWalkbox(tile.image)
                    topleft = tile.rect.topleft
                    contact[0] += topleft[0]
                    contact[1] += topleft[1]
                    if Physics.RectContains(contact, corner):
                        return False;
            
        return True

    def testPassable(self,actor,pos):
        walkbox = actor.props.getWalkbox(actor.image)
        pos = [int(pos[0]-walkbox[2]*.5),(pos[1]-walkbox[3])]
        size = walkbox[2:]
        
        corners = [pos,Physics.vectorAdd(pos, [size[0],0]),Physics.vectorAdd(pos, [0,size[1]]),Physics.vectorAdd(pos, size)]
        corners += [Physics.vectorAdd(pos, [size[0]*.5,size[1]]),Physics.vectorAdd(pos, [size[0],size[1]*.5]),Physics.vectorAdd(pos, [size[0]*.5,0]),Physics.vectorAdd(pos, [0,size[1]*.5]),Physics.vectorAdd(pos, [size[0]*.5,size[1]*.5])]
        for corner in corners:
            
            coords = self.getTileCoords(corner)
            if coords[0] == -1:
                return False
            tiles = [self.iTiles[coords[0]*2][coords[1]*2],self.iTiles[coords[0]*2][coords[1]*2+1],self.iTiles[coords[0]*2+1][coords[1]*2],self.iTiles[coords[0]*2+1][coords[1]*2+1]]
            for tile in tiles:
                if tile == None:
                    continue
                if (actor.props.getStepup() < tile.props.getHeight() or tile.props.getHeight() < 0):
                    contact = tile.props.getWalkbox(tile.image)
                    topleft = tile.rect.topleft
                    contact[0] += topleft[0]
                    contact[1] += topleft[1]
                    if Physics.RectContains(contact, corner):
                        return False;
        return True
    
        ## tileCoords = self.getTileCoords(pos)
        ## if tileCoords[0] < 0 or tileCoords[1] < 0:
        ##    return False
        ## tile = self.tiles[tileCoords[0]][tileCoords[1]]
        ## ret = True
        ## if (actor.props.getStepup() < tile.props.getHeight()):
        ##    ret = False
        ## print("Passable:",ret,tile.props.getHeight())
        ## return ret
        
    def removeActor(self,actor):
        try:
            self.actors.remove(actor)
        except ValueError:
            pass
        self.update = True
    def removeItem(self,item):
        try:
            self.items.remove(item)
        except ValueError:
            pass
        self.update = True
    def getPlayer(self):
        return self.actors[0]
    def getRow(self,coords):
        return int(coords[1]/self.tilesize[1])
    
    def getTileCoords(self,coords):
        if coords [0] < 0:
            return [-1,-1]
        if coords [1] < 0:
            return [-1,-1]
        
        tiley = int((coords[0])/self.tilesize[0][0])
        tilex = int((coords[1])/self.tilesize[0][1])
        try:
            tile = self.tiles[tilex][tiley]
        except IndexError:
            return [-1,-1]
        return [tilex,tiley] 
    def setActors(self,actors,spawn = "spawn"):
        self.actors=actors
        if (spawn == "spawn"):
            self.actors[0].setPosition(self.start)
    def setItems(self,items):
        self.items = items
            
    def clone(self):
        params = [copy.deepcopy(self.tiles),self.start,list(self.tilesize),list(self.connections),None,self.name]
        area = Area(params)
        area.setActors(self.actors)
        return area
    def getTilePosition(self, tileCoords, offset=[0,0], size=32):
        if size == 32:
            sizeIndex = 0
        else:
            sizeIndex = 1
        return [int(tileCoords[1]*self.tilesize[sizeIndex][0]+offset[0]),int(tileCoords[0]*self.tilesize[sizeIndex][1]+ offset[1])]
    
    def newArea(self,player):
        newAreaRet = None
        playerTile = self.getTileCoords(player.rect.midbottom)
        
        for i in self.connections.keys():
            if playerTile in self.connections[i][0]:
                newAreaRet = Area(buildMapFile(self.connections[i][1]))
                playerTile = self.connections[i][2].transform(playerTile)
                newPlayer = player.clone()
                newPlayer.rect.midbottom = self.getTilePosition(playerTile,[10,10])
                #sprint playerTile,newPlayer.rect.topleft
                newAreaRet.setActors([newPlayer],"nospawn")
                
                break
        return newAreaRet
    def getPath(self,actor,startPos,endPos,limit = -1):
        return Search.findTilePath(self,actor, startPos, endPos,limit)
        
def isTileIn(coords,areaArray):
        if coords[0] >= 0 and coords[0] < len(areaArray):
            if coords[1] >= 0 and coords[1] < len(areaArray[coords[0]]):
                return True
        return False
    
