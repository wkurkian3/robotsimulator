import Area
import LevelGenerator
import ImageProc
import Sprites
import PlayerModel
from level.agent.digging_agent import DiggingAgent
from level.agent.entrance_agent import EntranceAgent
from level.agent.enemy_agent import EnemyAgent
from level.agent.connecting_agent import ConnectingAgent
from level.agent.smoothing_agent import SmoothingAgent
from level.agent.treasure_agent import TreasureAgent
import Logger

class AgentLevelGenerator(LevelGenerator.LevelGenerator):
    def __init__(self):
        pass

    def create_agents(self, area, width, height, current_level, player_model, actors, items, size):
        agents = []
        agents.append(DiggingAgent(0, 0, width, height, area, width, height, player_model))
        agents.append(SmoothingAgent(area, width, height, player_model))
        agents.append(ConnectingAgent(area, width, height, player_model))
        agents.append(EntranceAgent(area, width, height, player_model))
        agents.append(TreasureAgent(area, width, height, player_model, current_level, items, size))
        agents.append(EnemyAgent(area, width, height, player_model, current_level, actors, size))
        return agents

    def process_map(self, area, size, tiledict):
        start = [5*size[0],5*size[1]]
        for y in range(len(area)):
            for x in range(len(area[y])):
                if (area[y][x] == '+'):
                    area[y][x] = 3
                    start = [x*size[0], y*size[1]]
                pos = [x*size[0],y*size[1]]
                image = tiledict[str(area[y][x])]
                props = tiledict[image]
                layer = Area.detLayer(pos,size,props)
                area[y][x] = Sprites.Tile(pos,image,props,layer)
        return start

    def createLevel(self,player,playerModel,currentLevel):
        tiledict = ImageProc.getTileset('walls')
        connections = {}
        size = tiledict['size']
        width = 40
        height = 30
        area = [[1 for x in range(width)] for y in range(height)]

        # playerModel = PlayerModel.testMeleeDescription()
        #playerModel = PlayerModel.testRangedDescription()

        actors = []
        items = []
        agents = self.create_agents(area, width, height, currentLevel, playerModel, actors, items, size)
        for agent in agents:
            agent.execute()

        start = self.process_map(area, size, tiledict)
        Logger.totalEnemies(len(actors))
        actors.insert(0, player)

        areaMap = Area.Area([area,start,size,connections,tiledict,''])
        areaMap.setActors(actors)
        areaMap.setItems(items)
        return areaMap
