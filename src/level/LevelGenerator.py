import Area
import ImageProc

class LevelGenerator:
    def __init__(self):
        raise NotImplementedError("Subclasses should implement this!")
        #do nothing for now
    def createLevel(player,playerModel,currentLevel):
        #raise NotImplementedError("Subclasses should implement this!")
        #Area.buildMap might be helpful to look at for this.
        #It builds a map from the textfiles in the map folder.
        
        #this indicates which set of tiles you are using. Currently only walls exists.
        #Look at image/walls.txt and images/wall.png for more information.
        tiledict = ImageProc.getTileset('walls')
        connections = {}# leave empty for now. This feature needs work
        start = [0,0] #this should be some x,y position. it is col*size[0] x row*size[1]
        size = tiledict['size'] #size of the tiles. This comes from a tileset
        
        #this is a row by column tile map where you fill in the type of tiles
        #note that it must be rectangular, so empty spaces must have blank spaces
        #get blank by using a blank image - get it by: image = tiledict['-']
        area = [[]]
        
        #example tile add
        #pos is the x,y position. do it the same way as the player start position
        #image is the image used obviously
        #Get it by: image = ImageProc.getEntry(tiledict, index)
        # where index is a number from walls.txt. We should add constants to make this easier.
        #props is the object properties
        #props = tiledict[image]
        #layer is important for getting z indexing right.
        #layer = Area.getLayer(pos,size,props)
        area[0].append(Sprites.Tile(pos,image,props,layer))
        #You need to make an area object based on the above information. Enemies can add afterwards for now
        areaMap = Area.Area([area,start,size,connections,tiledict,''])
        return areaMap
                       
                      
