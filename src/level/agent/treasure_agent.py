from level.agent.agent import Agent
import random
import item.Item as Item
import item.Consumable as Consumable
import Logger

class TreasureAgent(Agent):

    def __init__(self, area, width, height, player_model, current_level, items, size):
        super(TreasureAgent, self).__init__(area, width, height, player_model)
        self.current_level = current_level
        self.items = items
        self.size = size

    def execute(self):
        num = 3 + self.current_level
        while (num > 0):
            x = random.randint(2, self.width-2)
            y = random.randint(2, self.height-2)
            if (self.area[y][x] == 3):
                r = random.random()
                if (self.player_model.meleePref > self.player_model.rangedPref):
                    stat = Consumable.SPEED
                    if (r < 0.5):
                        stat = Consumable.HEALTH
                    elif (r < 0.75):
                        stat = Consumable.ATTACK
                else:
                    stat = Consumable.SPEED
                    if (r < 0.3):
                        stat = Consumable.HEALTH
                    elif (r < 0.5):
                        stat = Consumable.ATTACK
                pickup = Item.buildStatUpPickup(stat)
                pickup.rect.topleft = [x * self.size[0], y * self.size[1]]
                self.items.append(pickup)
                num -= 1
        num = 1
        attack = random.randint(self.current_level - 5, self.current_level + 5)
        speed = random.randint(self.current_level - 5, self.current_level + 5)
        shield = random.randint(self.current_level - 2, self.current_level + 2)
        hook = random.randint(self.current_level - 2, self.current_level + 2)
        if (attack < 1): attack = 1
        if (speed < 1): speed = 1
        if (hook < 1): hook = 1
        if (shield < 1): shield = 1
        items = [Item.buildBowPickup(attack, speed), Item.buildSwordPickup(attack, speed),
                Item.buildGrapplingHookPickup(hook), Item.buildShieldPickup(speed)]
        while (num > 0):
            x = random.randint(2, self.width-2)
            y = random.randint(2, self.height-2)
            if (self.area[y][x] == 3):
                pickup = items[random.randint(0, 2)]
                pickup.rect.topleft = [x * self.size[0], y * self.size[1]]
                self.items.append(pickup)
                Logger.addItemCount(pickup.item.type)
                num -= 1
