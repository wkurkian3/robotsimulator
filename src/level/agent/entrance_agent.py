from level.agent.agent import Agent
import random

class EntranceAgent(Agent):
    def execute(self):
        n = (-1, -1)
        s = (-1, -1)
        w = (self.width, 0)
        e = (0, 0)
        for y in range(self.height):
            for x in range(self.width):
                if (self.area[y][x] == 3):
                    if (n[0] == -1):
                        n = (x, y)
                    s = (x, y)
                    if (x < w[0]):
                        w = (x, y)
                    if (x > e[0]):
                        e = (x, y)
        pairs = [(n,s), (w,e), (s, n), (e, w)]
        d = random.randint(0,3)
        self.area[pairs[d][0][1]][pairs[d][0][0]] = '+'
        self.area[pairs[d][1][1]][pairs[d][1][0]] = '>'
