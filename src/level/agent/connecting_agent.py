from level.agent.agent import Agent
import math

class ConnectingAgent(Agent):

    def execute(self):
        self.mark_regions()
        while (self.counter > 1):
            mind = None
            for i in range(1, self.counter+1):
                for j in range(i + 1, self.counter+1):
                    d = self.closest_points(i, j)
                    if (mind is None):
                        mind = d
                    elif (d[2] < mind[2]):
                        mind = d
            self.connect(mind[0][0], mind[0][1], mind[1][0], mind[1][1])
            self.mark_regions()

    def mark_regions(self):
        self.region_map = [[-1 for x in range(self.width)] for y in range(self.height)]
        self.counter = 0
        self.unmarked = self.width * self.height
        while (self.unmarked > 0):
            for y in range(self.height):
                for x in range(self.width):
                    if (self.area[y][x] == 1):
                        self.region_map[y][x] = 0
                        self.unmarked -= 1
                    elif (self.region_map[y][x] == -1):
                        self.counter += 1
                        self.mark(x, y, self.counter)

    def mark(self, x, y, region):
        if (x >= 0 and y >= 0 and x < self.width and y < self.height and self.area[y][x] == 3 and self.region_map[y][x] == -1):
            self.region_map[y][x] = region
            self.unmarked -= 1
            self.mark(x-1, y, region)
            self.mark(x+1, y, region)
            self.mark(x, y-1, region)
            self.mark(x, y+1, region)

    def closest_points(self, region1, region2):
        points1 = self.border_points(region1)
        points2 = self.border_points(region2)
        p1 = points1[0]
        p2 = points2[0]
        mind = self.euclid(p1, p2)
        for i in range(1, 4):
            for j in range(1, 4):
                d = self.euclid(points1[i], points2[j])
                if (d < mind):
                    mind = d
                    p1 = points1[i]
                    p2 = points2[j]
        return (p1, p2, mind)

    def border_points(self, region):
        n = (-1, -1)
        s = (-1, -1)
        w = (self.width, 0)
        e = (0, 0)
        for y in range(self.height):
            for x in range(self.width):
                if (self.region_map[y][x] == region):
                    if (n[0] == -1):
                        n = (x, y)
                    s = (x, y)
                    if (x < w[0]):
                        w = (x, y)
                    if (x > e[0]):
                        e = (x, y)
        return (n, s, w, e)

    def connect(self, x1, y1, x2, y2):
        dx = 1 if x1 < x2 else -1
        dy = 1 if y1 < y2 else -1
        while (x1 != x2 or y1 != y2):
            if (x1 != x2):
                x1 += dx
                self.area[y1][x1] = 3
            if (y1 != y2):
                y1 += dy
                self.area[y1][x1] = 3

    def euclid(self, p1, p2):
        return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
