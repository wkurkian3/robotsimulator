import Physics

def move(person,area,direction,step=5):
    if (area.canMove(person,direction,step)):
        person.move(direction,step)
    
def useItem(person,area):
    person.mainAction(area)

def pickupItem(person,area):
    person.pickup(area)

def useSideItem(person,area):
    person.sideAction(area)
    
