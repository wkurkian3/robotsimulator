import random

class GeneticAlgorithm:
    instances = []
    def __init__(self,start,evaluation,mutation,crossover):
        for z in range(100):
            self.instances += [mutation(start)]
        self.mutation = mutation
        self.crossover = crossover
        self.evaluation = evaluation
    def solve(self,iterations):
        for z in range(iterations):
            check = []
            #for i in self.instances:
            #    check += [len(i.sectors)]
            #print check
            fitnesses = []
            for i in self.instances:
                fitnesses += [self.evaluation(i)]
        
            self.instances = [x for (y,x) in sorted(zip(fitnesses,self.instances))]
            #self.instances.reverse()
            for _ in range(len(self.instances)-20):
                self.instances.pop(0)
            for _ in range(20):
                inst1 = random.randint(0,19)
                inst2 = random.randint(0,19)
                inst3 = self.crossover(self.instances[inst1],self.instances[inst2])
                self.instances.append(inst3)
            
            while len(self.instances) <100:
                inst1 = random.randint(0,39)
                self.instances += [self.mutation(self.instances[inst1])]
        return self.instances[0]
                
                
